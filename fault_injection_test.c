/*
 * CFile1.c
 *
 * Created: 3/16/2015 13:45:52
 *  Author: ZhouPeng
 */ 

#include "detection.h"
#include "hwLED.h"
#include <util/delay.h>
#include <stdbool.h>
#include <stdlib.h>
#include "recovery.h"
#include <avr/eeprom.h>

//extern int test_counter;
volatile int cc=0;
typedef void(*pfunction)(void);


bool recvery_BlueLED(void)
{
	uint8_t i=0;
	
	while(i++ < 5)
	{
		HAL_GPIO_BlueLED_set();
		_delay_ms(100);
		HAL_GPIO_BlueLED_clr();
		_delay_ms(100);
	}
	return true;
}
//3)	Random PC Error
void process_pc(void * args)
{
	while(1)
	{
		pfunction error =(pfunction)((uint16_t) (rand()% 0xFFFF)) ;// here, we do not modify the RAMPZ, it will increase 128K/64K times of the failure rate. 

		HAL_GPIO_GreenLED_set();
		_delay_ms(100);
		HAL_GPIO_GreenLED_clr();
		_delay_ms(50);
		_delay_ms(2000);
		error();
	}
}
//simulate the WCET violation
void process_wcet(void * args)
{
	while(1)
	{
		HAL_GPIO_GreenLED_set();
		_delay_ms(100);
		HAL_GPIO_GreenLED_clr();
		_delay_ms(50);
	}
}
//2)Random SRAM Modify 
void process_Data_Error(void * args)
{
	int i=0;
	while(i++<18)
	{
		HAL_GPIO_GreenLED_set();
		_delay_ms(100);
		HAL_GPIO_GreenLED_clr();
		_delay_ms(100);
	}
	*(((uint8_t*)((rand()% 0x1000)))) =(uint8_t) (rand()% 0xFF);
	_delay_ms(1000);
}

void initrand()
{
	uint16_t state;
	static uint32_t EEMEM sstate;

	state = eeprom_read_dword(&sstate);

	// Check if it's unwritten EEPROM (first time). Use something funny
	// in that case.
	if (state == 0xffff)
	state = 0xDEADUL;
	srand(state);
	eeprom_write_dword(&sstate, rand());
}

int main (void)
{
	MCUSR = 0;
	CLKPR = 1 << CLKPCE;
	CLKPR = 0;
	wdt_disable();
	SYS_EnableInterrupts();

	InitAllLed();
	_delay_ms(100);
	HAL_GPIO_RedLED_set();
	_delay_ms(100);
	HAL_GPIO_RedLED_clr();
	//~0.5s delay
	_delay_ms(100);
	
	uint8_t cnt=0;
	//to ensure every testing is different after system restarting, to guarantee the Randomness of error
	initrand();
	while(1){//testing
		Container(process_wcet, recvery_BlueLED,4000, &cnt);
	}
	
	//it will never be excuted, just in case the container recovery fails.
	while(1)
	{
		HAL_GPIO_RedLED_set();
		_delay_ms(50);
		HAL_GPIO_RedLED_clr();
		//~0.5s delay
		_delay_ms(100);
	}
}
