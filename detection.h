/**
 *!!!warring: Timer/counter is monopolized, it should not be used for other purpose,
 *!!!warring: this function is no Thread safety!!!!!!!
 *or unknown behavior may occur.
 *2015-3-16 21:04:45
 */

#ifndef _DETECTION_RECOVERY_H_
#define _DETECTION_RECOVERY_H_

/*- Includes ---------------------------------------------------------------*/
#include "SysConfig.h"
#include <stdbool.h>
#include <setjmp.h>

typedef bool (*pfun_action)(void);

typedef union{
	uint16_t u16;
	uint8_t data[2];	
} union16;


typedef enum {
	ACTION_RESTART,
	ACTION_RECOVERY,
}Failuer_Action;

/*- Prototypes -------------------------------------------------------------*/
bool Daemon_Restart_Init(uint16_t ms, pfun_action inform);
bool Daemon_Recover_Init(uint16_t ms, pfun_action recover_op, jmp_buf* rev_point, int value);
void Daemon_Stop(void);
#endif // _DETECTION_RECOVERY_H_
