/**
 * \file halTimer.c
 *
 * \brief ATmega128rfa1 timer implementation
 *
 * Copyright (C) 2012-2014, Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 * Modification and other use of this code is subject to Atmel's Limited
 * License Agreement (license.txt).
 *
 * $Id: halTimer.c 9267 2014-03-18 21:46:19Z ataradov $
 *
 */

/*- Includes ---------------------------------------------------------------*/

#include "detection.h"
//#include <util/delay.h>
#include <stddef.h>
#include "hwWatchDog.h"
//#include "scm_hsdtvi.hi"

# warning "when this detect recovery module included, Timer/counter4  should not be used for other purpose"
/*- Definitions ------------------------------------------------------------*/
#define TIMER_PRESCALER     1024

/*- Variables --------------------------------------------------------------*/
static volatile uint8_t* timer_sp;
static volatile pfun_action pfun_op;
static jmp_buf* recover_point;
static int revalue;
static Failuer_Action aType;

//volatile int test_counter =1 ;
/*- Implementations --------------------------------------------------------*/

/*************************************************************************//**
*****************************************************************************/

bool Daemon_Restart_Init(uint16_t ms, pfun_action inform)
{
	if(NULL!= inform ) 
	{
		pfun_op = inform;
		TCCR4A = 0;
		TCCR4B = 0;
		OCR4A = (F_CPU / 1000ul / TIMER_PRESCALER) * ms;
		TCCR4B = (1 << WGM42);              // CTC mode
		TCCR4B |= (1 << CS42)|(1 << CS40);  // Prescaler 1024
		TIMSK4 |= (1 << OCIE4A);            // Enable TC4 interrupt
		aType = ACTION_RESTART;
		return true;
	}else return false;
}

bool Daemon_Recover_Init(uint16_t ms, pfun_action recover_op, jmp_buf* rev_point, int value)
{
	if(NULL!= recover_op )
	{
		pfun_op = recover_op;
		TCCR4A = 0;
		TCCR4B = 0;
		OCR4A = (F_CPU / 1000ul / TIMER_PRESCALER) * ms;
		TCCR4B = (1 << WGM42);              // CTC mode
		TCCR4B |= (1 << CS42)|(1 << CS40);     // Prescaler 1024
		TIMSK4 |= (1 << OCIE4A);            // Enable TC4 interrupt
		aType = ACTION_RECOVERY;
		recover_point = rev_point;
		revalue = value;
		return true;
	}else return false;
}

inline void Daemon_Stop(void)
{
	TIMSK4 &= ~(1 << OCIE2A);
	TCCR4B &= 0B11111000;
}
void action (void)
{
	//for(uint8_t i =0; i< 10 ; i++)
	//{
		//HAL_GPIO_RedLED_set();
		//for(uint16_t j =0; j< 36800 ; j++);
		//HAL_GPIO_RedLED_clr();
		//for(uint16_t j =0; j< 36800 ; j++);
	//}
		
	TIMSK4 &= ~(1 << OCIE2A);
	TCCR4B &= 0B11111000;
	//Watchdog_init(WDTO_4S);

	if( pfun_op())
	{

		if(  ACTION_RECOVERY== aType )
		{
			 wdt_disable();
			 revalue =(0 == revalue) ? revalue : revalue + 1;
		
			 longjmp(*recover_point, revalue);
		}else{//restart
			Watchdog_init(WDTO_15MS);
		}
	}
	else{
		//restart the system if recovery failed
		//or do something;
		Watchdog_init(WDTO_15MS);
	}
	
}

#if defined(INCREASING_STACK) //little bottom
ISR(TIMER4_COMPA_vect)
{
	//test_counter ++;
#ifdef __OPTIMIZE__
	timer_sp = (uint8_t*) SP - OPTIMIZED_STACK_STEP;//
#else
	timer_sp = (uint8_t*) SP - GENERAL_STACK_STEP;//14
#endif
	union16  address;
	address.u16 = (uint16_t)action;
	
#if defined(BIG_END_CALL_RETURN_STACK)
	*((uint8_t*)timer_sp) =address.data[1];
	*((uint8_t*)(timer_sp-1)) =(uint8_t)address.data[0] ;
#elif defined(LITTLE_END_CALL_RETURN_STACK)
	*((uint16_t*)timer_sp) =address.u16;
	//*((uint8_t*)(timer_sp+1)) =(uint8_t)address.data[1] ;
#else
	  #error "Unknown CALL_RETURN_STACK type"
#endif
}

#else //DECREASING_STACK, big bottom
ISR(TIMER4_COMPA_vect)
{
#ifdef __OPTIMIZE__
	timer_sp = (uint8_t*) SP + OPTIMIZED_STACK_STEP;//
#else
	timer_sp = (uint8_t*) SP + GENERAL_STACK_STEP;//14
#endif
	union16  address;
	address.u16 = (uint16_t)action;
	
#if defined(BIG_END_CALL_RETURN_STACK)
	*((uint8_t*)timer_sp) =address.data[1];
	*((uint8_t*)(timer_sp+1)) =(uint8_t)address.data[0] ;
#elif defined(LITTLE_END_CALL_RETURN_STACK)
	*((uint16_t*)timer_sp) =address.u16;
	//*((uint8_t*)(timer_sp+1)) =(uint8_t)address.data[1] ;
#else
	  #error "Unknown CALL_RETURN_STACK type"
#endif
}
#endif
