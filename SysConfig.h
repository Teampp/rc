/*
 * sysConfig.h
 *
 * Created: 1/27/2015 11:17:06
 *  Author: ZhouPeng
 */ 


#ifndef SYSCONFIG_H_
#define SYSCONFIG_H_

#include <stdint.h>
#define  BIT_LENGTH 8

#if 1
#define __ALIGNED2     __attribute__ ((aligned (2)))
#else
#define __ALIGNED2
#endif

#define SYS_LW_MESH_ENV

#if defined(__ICCAVR__)
  #include <inavr.h>
  #include <ioavr.h>
  #include <intrinsics.h>
  #include <pgmspace.h>

  #define SYS_MCU_ARCH_AVR

  #define PACK

  #define PRAGMA(x) _Pragma(#x)

  #define INLINE PRAGMA(inline=forced) static

  #define SYS_EnableInterrupts() __enable_interrupt()

  #define wdt_reset() (__watchdog_reset())

  #define wdt_enable(timeout) do { \
     uint8_t __atomic = SREG; __disable_interrupt(); \
    __watchdog_reset(); \
    WDTCSR |= (1 << WDCE) | (1 << WDE); \
    WDTCSR = (1<<WDIE)|(0<<WDE) | timeout; \ __enable_interrupt(); \
    SREG = __atomic; \
  } while (0)

  #define wdt_disable() do { \
    MCUSR = 0; \
    WDTCSR |= (1 << WDCE) | (1 << WDE); \
    WDTCSR = 0x00; \
  } while (0)

  #define ISR(vec) PRAGMA(vector=vec) __interrupt void handler_##vec(void)

  #define ATOMIC_SECTION_ENTER   { uint8_t __atomic = SREG; __disable_interrupt();
  #define ATOMIC_SECTION_LEAVE   SREG = __atomic; }
/*
#elif defined(__ICCARM__)
  #error Unsupported compiler

#elif defined(__ICCAVR32__)
  #error Unsupported compiler
*/
#else
  #if defined(HAL_ATSAMD20J18)
    #define DONT_USE_CMSIS_INIT
    #include "samd20j18.h"

    #define SYS_MCU_ARCH_CORTEX_M

  #elif defined(HAL_ATSAMD21J18)
    #define DONT_USE_CMSIS_INIT
    #include "samd21j18a.h"

    #define SYS_MCU_ARCH_CORTEX_M

  #else // All AVRs
    #include <avr/io.h>
    #include <avr/wdt.h>
    #include <avr/interrupt.h>
    #include <avr/pgmspace.h>

    #define SYS_MCU_ARCH_AVR
  #endif

  #if defined(SYS_MCU_ARCH_CORTEX_M)
    #define PRAGMA(x)

    #define PACK __attribute__ ((packed))

    #define INLINE static inline __attribute__ ((always_inline))

    #define SYS_EnableInterrupts() __asm volatile ("cpsie i");

    #define ATOMIC_SECTION_ENTER   { register uint32_t __atomic; \
                                     __asm volatile ("mrs %0, primask" : "=r" (__atomic) ); \
                                     __asm volatile ("cpsid i");
    #define ATOMIC_SECTION_LEAVE   __asm volatile ("msr primask, %0" : : "r" (__atomic) ); }

  #elif defined(SYS_MCU_ARCH_AVR)
    #define PRAGMA(x)

    #define PACK __attribute__ ((packed))

    #define INLINE static inline __attribute__ ((always_inline))

    #define SYS_EnableInterrupts() sei()

    #define ATOMIC_SECTION_ENTER   { uint8_t __atomic = SREG; cli();
    #define ATOMIC_SECTION_LEAVE   SREG = __atomic; }

  #endif

#endif

#if defined(__AVR_ATmega1280__ )|| defined(__AVR_ATmega1281__) ||defined(__AVR_ATmega128RFA1__)
	#define SYS_DEVICE_SIZE			131072
	#define SYS_PAGE_SIZE			256
	#define SYS_BOOTLOADER_SIZE		2048
	#define BIG_END_CALL_RETURN_STACK
	#define OPTIMIZED_STACK_STEP	11
	#define GENERAL_STACK_STEP		14
	#define DECREASING_STACK
 
#elif defined(__AVR_ATmega2560__)
	#define SYS_DEVICE_SIZE			262144
	#define SYS_PAGE_SIZE			256
	#define SYS_BOOTLOADER_SIZE		2048
	#define BIG_END_CALL_RETURN_STACK  //LITTLE_END_CALL_RETURN_STACK
	#define OPTIMIZED_STACK_STEP	12
	#define GENERAL_STACK_STEP		15
	#define DECREASING_STACK
#else
  #error Unknown HAL
#endif

#endif /* SYSCONFIG_H_ */