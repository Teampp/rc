/*
 * hwWatchDog.c
 *
 * Created: 2/16/2015 16:03:48
 *  Author: ZhouPeng
 */ 
#include <avr/interrupt.h>
//#include <util/delay.h>
#include "hwWatchDog.h"
static uint8_t restart_counter=0;
//when triggered, system check_and recovery will be started
//extern 

ISR(WDT_vect){
	//do some to storage the important message
	restart_counter++;
	//HAL_GPIO_RedLED_set();
	//for(uint16_t i=0; i < 65534; i++);
	//HAL_GPIO_RedLED_clr();
	//for(uint16_t i=0; i < 65534; i++);
	//die peacefully, 1 s later, you are still a great system ^_^ !
}

void Watchdog_init(uint8_t val ){
	//disable interrupts
	cli();
	//make sure watchdod will be followed by a reset (must set this one to 0 because it resets the WDE bit)
	MCUSR&=~(1<<WDRF);
	WDTCSR |= (1<<WDCE)|(1<<WDE);
	//Start watchdog timer with val prescaller and interrupt only
	if(val > WDTO_2S)
	{
		WDTCSR = (1 <<WDIE)|(1<<WDE)|(1<<WDP3)| (val-WDTO_2S);
		//WDTCSR = (0 <<WDIE)|(1<<WDE)|(0<<WDP3)| (val-WDTO_2S);//just reset ,don't store message
	}else{
		WDTCSR = (1 <<WDIE)|(1<<WDE)| val;
		//WDTCSR = (0 <<WDIE)|(1<<WDE)| val; //just reset ,don't store message
	}
	//	wdt_disable();
	sei();
}


/*void Watchdog_init(){
	//README : must set the fuse WDTON to 0 to enable the watchdog
	
	//disable interrupts
	cli();
	
	//make sure watchdod will be followed by a reset (must set this one to 0 because it resets the WDE bit)
	MCUSR &= ~(1 << WDRF);
	//set up WDT interrupt (from that point one have 4 cycle to modify WDTCSR)
	//WDTCSR = (1<<WDCE)|(1<<WDE);
	//Enable global interrupts
	sei();
}*/
