/*
 * @file recovery.h
 * @warning it just behave the same with Container when try_amount==0, and this Container_Redo dosen't guarantee the success of redo 
 * Created: 3/17/2015 10:29:51
 * @author: ZhouPeng
 */ 


#ifndef RECOVERY_H_
#define RECOVERY_H_
#include "detection.h"

typedef void (*pfun_process)(void* args);
//void Container (pfun_process process, pfun_action recovery);
//void Container (pfun_process process, pfun_action recovery, uint16_t deadline_ms );
void Container (pfun_process process,pfun_action recovery, uint16_t deadline_ms, void* args);
void Container_Redo (pfun_process process, pfun_action recovery, uint16_t deadline_ms, void* args, uint8_t try_amount);
#endif /* RECOVERY_H_ */