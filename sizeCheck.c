/*
 * size.c
 *
 * Created: 3/22/2016 19:55:41
 *  Author: zhoupeng
 */ 

#include <stdbool.h>
#include <avr/io.h>
#include "recovery.h"

typedef void(*pfunction)(void);
bool recovery(void){};
void process(void * args){};
		
int main(void)
{
	MCUSR = 0;
	CLKPR = 1 << CLKPCE;
	CLKPR = 0;
	//wdt_disable();
	Container(process, recovery,2000, 0);
}