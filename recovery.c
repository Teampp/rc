/*
 * @file recovery.c
 *
 * @date Created: 3/17/2015 10:30:08
 * @author: ZhouPeng
 */ 

#include "recovery.h"
#include <setjmp.h>
#include <util/delay.h>
static jmp_buf  env;

/************************************************************************
* @param param1  process, the program to monitor
* @param param2 recovery, the recovery function when process failed
************************************************************************/
void Container (pfun_process process,pfun_action recovery, uint16_t deadline_ms, void* args)
{
	int rev= setjmp (env);
	if(rev>0)
	{
		//do something if needs;
		//like releasing the resource holded by proces() if the system supports dynamic memory allocation
		return;
	}else{
		Daemon_Recover_Init(deadline_ms,recovery, &env, rev);
		process(args);
		Daemon_Stop();
	}
}
/************************************************************************
* @param param1  process, the program to monitor
* @param param2 recovery, the recovery function when process failed
* @param param3 try_amount, the amount to redo the process, but generally it:s not recommended 
* @warning it just behave the same with Container when try_amount==0, and this Container_Redo dosen't guarantee the redo 
************************************************************************/
void Container_Redo (pfun_process process, pfun_action recovery, uint16_t deadline_ms, void* args, uint8_t try_amount )
{
	int rev= setjmp (env);
	if(rev>try_amount)
	{
		//do something if needs; 
		//like release the resource holded by proces() if the system supports dynamic memory allocation
		return;
	}else{
		Daemon_Recover_Init(deadline_ms,recovery, &env, rev);
		process(args);
		Daemon_Stop();
	}
}
