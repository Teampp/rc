/*
 * hwWatchDog.h
 *
 * Created: 2/16/2015 16:04:09
 *  Author: ZhouPeng
 */ 


#ifndef HWWATCHDOG_H_
#define HWWATCHDOG_H_
#include "SysConfig.h"
extern  volatile uint8_t dy;
void Watchdog_init(uint8_t val );

typedef struct {
	uint16_t stack_pointer;
	uint16_t process_counter;
	uint16_t state_register;
}avr_context;


#endif /* HWWATCHDOG_H_ */